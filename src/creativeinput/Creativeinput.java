/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package creativeinput;
import java.io.IOException;
//import java.io.Writer;
/**
 *
 * @author David Shustin
 */
public class Creativeinput {
    
    /**
     * @param args the command line arguments
     */
    static String readInput () {
        String x = "";
        int curr;
        try {
            while (System.in.available() == 0); // wait for input
            while (System.in.available() > 0) {
                curr = System.in.read();
                if ((char)curr != '\n')
                    x += Character.toString((char)curr);
            }
        } catch (IOException e) {
            System.err.println("An IOException occured.");
            return "ERROR";
        }
        return x;
    }
    public static void main(String[] args) {
        // TODO code application logic here
        String name = "";
        String[] nums = new String[3];
        int[] ints = new int[3];
        boolean isValid= true;
        System.err.print("What is your name?\nName: ");
        name = readInput();
        System.err.println("Hello, " + name + "!");
        for (int i = 0; i < 3; i++) {
            do {
                System.err.print("Please enter a valid integer for score #" + (i+1) + ": ");
                nums[i] = readInput();
                try {
                    ints[i] = Integer.parseInt(nums[i]);
                    isValid = true;
                } catch (NumberFormatException e) {
                    isValid = false;
                }
                if (ints[i] <= 0) {
                    isValid = false;
                }
                //System.err.println(ints[i]);
            } while (!isValid);
        }
        System.err.println("The average of your numbers is: " + ((ints[0] + ints[1] + ints[2])/3.0));
    }
    
}
